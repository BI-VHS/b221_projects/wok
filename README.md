# WOK

Další či aktuálnější soubory jsou na [Google Drive](https://drive.google.com/drive/folders/1--4GsCKFjD9EK_DiZ8UTJbW5rv_2vEpa?usp=sharing)!

---

The (always work-in-progress) game design document is in the [Google Drive shared folder](https://drive.google.com/drive/folders/1--4GsCKFjD9EK_DiZ8UTJbW5rv_2vEpa?usp=sharing). Accessible freely to anyone `@fit.cvut.cz`.

